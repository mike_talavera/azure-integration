<?php
/*
Plugin Name: azure
Description: Azure connection
Version: 1.0
Author: Mike Gutierrez
License: GPLv2 or later
Text Domain: azure
*/
define( 'AZURE__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
require_once( AZURE__PLUGIN_DIR . 'class.azureapi.php' );
require_once( AZURE__PLUGIN_DIR . 'class.callback.php' );
require_once( AZURE__PLUGIN_DIR . 'config.php' );

add_action( 'plugins_loaded', array( new AzureApi, 'init' ) );


// One time activation functions
register_activation_hook( AZURE__PLUGIN_DIR, array( new AzureApi, 'flush_rules' ) );


function azure_startSession() {
    if(!session_id()) {
        session_start();
    }
}

add_action('init', 'azure_startSession', 1);

add_action('template_redirect', "azure_redirect", 1);

function azure_redirect() {
    if ( true === strpos( $_SERVER['REQUEST_URI'], '/wp-login' ) ) {
       wp_redirect( "/account/loginazure", 301 );
       exit;
    }
}

add_action('init','custom_login');

function custom_login(){
 global $pagenow;
 if( 'wp-login.php' == $pagenow && $_GET['action']!="logout") {
  wp_redirect($app["root_wordpress"] . 'account/loginazure');
  exit();
 }

 if( 'wp-login.php' == $pagenow && $_GET['action']=="logout") {    
    wp_redirect("/account/logout");
    exit();
 }
}