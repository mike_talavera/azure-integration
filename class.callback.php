<?php


/**
 * 
 */
class Callback
{
	
	public function init()
    {
        add_filter( 'template_include', array( $this, 'include_template' ) );
        add_filter( 'init', array( $this, 'rewrite_rules' ) );
    }

    public function include_template( $template )
    {
        //try and get the query var we registered in our query_vars() function
        $account_page = get_query_var( 'callback' );

        //if the query var has data, we must be on the right page, load our custom template
        if ( $account_page ) {
            return AZURE__PLUGIN_DIR ."views/" . 'callback.php';
        }

        return $template;
    }

    public function flush_rules()
    {
        $this->rewrite_rules();

        flush_rewrite_rules();
    }

    public function rewrite_rules()
    {
        add_rewrite_rule( 'accountcallback/(.+?)/?$', 'index.php?callback=$matches[1]', 'top');
        add_rewrite_tag( '%callback%', '([^&]+)' );
    }
}