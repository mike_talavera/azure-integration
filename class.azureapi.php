<?php

/**
 * 
 */
require 'vendor/autoload.php';

class AzureApi
{
	
	public function init()
    {
        add_filter( 'template_include', array( $this, 'include_template' ) );
        add_filter( 'init', array( $this, 'rewrite_rules' ) );
    }

    public function include_template( $template )
    {
        //try and get the query var we registered in our query_vars() function
        $account_page = get_query_var( 'loginazure' );

        //if the query var has data, we must be on the right page, load our custom template
        if ( $account_page == "loginazure") {
            return AZURE__PLUGIN_DIR ."views/" . 'login.php';
        }
        if ( $account_page == "callback") {
            return AZURE__PLUGIN_DIR ."views/" . 'callback.php';
        }
        if ( $account_page == "logout") {
            return AZURE__PLUGIN_DIR ."views/" . 'logout.php';
        }
        
        //return AZURE__PLUGIN_DIR ."views/" . 'login.php';
        return $template;
    }

    public function flush_rules()
    {
        $this->rewrite_rules();

        flush_rewrite_rules();
    }

    public function rewrite_rules()
    {
        add_rewrite_rule( 'account/(.+?)?$', 'index.php?loginazure=$matches[1]', 'top');
        add_rewrite_tag( '%loginazure%', '([^&]+)' );
    }
}