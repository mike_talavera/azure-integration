<?php
	use Microsoft\Graph\Graph;
	use Microsoft\Graph\Model;
	

	$provideEstate = $_GET["state"];
	if (!isset($_SESSION["oauthState"])) {
      // If there is no expected state in the session,
      // do nothing and redirect to the home page.
        if ( wp_redirect( $app["root_wordpress"] ) ) {
    		exit;
		}
    }
	$expectedState = $_SESSION["oauthState"];
	
	unset($_SESSION['oauthState']);
	if(!$provideEstate || $provideEstate != $expectedState){
		if ( wp_redirect( $app["root_wordpress"] ) ) {
    		exit;
		}		
	}
	$authCode = $_GET["code"];

	if(isset($authCode)){		
		$oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
	      'clientId'                => $app["clientId"],
	      'clientSecret'            => $app["clientSecret"],
	      'redirectUri'             => $app["redirectUri"],
	      'urlAuthorize'            => $app["auth"].$app["tennant"].$app["urlAuthorize"],
	      'urlAccessToken'          => $app["auth"].$app["tennant"].$app["urlAccessToken"],
	      'urlResourceOwnerDetails' => $app["urlResourceOwnerDetails"],
	      'scopes'                  => $app["scopes"],
	    ]);
	    try {
        // Make the token request
	        $accessToken = $oauthClient->getAccessToken('authorization_code',
	        	[
	          'code' => $authCode
	        ]);
	        $graph = new Graph();
		  	$graph->setAccessToken($accessToken->getToken());
		  	$user = $graph->createRequest('GET', '/me?$select=displayName,mail,userPrincipalName')
		    ->setReturnType(Model\User::class)
		    ->execute();
		    $username = $user->getUserPrincipalName();
			

			// Redirect URL //
			$result ="";
			if(!username_exists($username)){				
				$result = wp_create_user(
					$user->getUserPrincipalName(), 
					'pragma', 
					$user->getUserPrincipalName()
				);
			}
			$wp_user = get_user_by('login', $username );
			
			if ( !is_wp_error( $wp_user ) )
			{
			    wp_clear_auth_cookie();
			    wp_set_current_user ( $wp_user->ID );
			    wp_set_auth_cookie  ( $wp_user->ID );
				
			    //$redirect_to = user_admin_url();
			    print_r("if $username");
			    wp_redirect($app["root_wordpress"]);
			    exit();
			}
		    	
			print_r($result);
	       	//wp_redirect($app["root_wordpress"]);
			exit();
		}
		catch (GuzzleHttp\Exception\ClientException $e){
			print_r($e->getMessage());
		}
		catch (League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
		 	print_r("Error con el token" . "<br>");   	
		 	if ( wp_redirect( $app["root_wordpress"] ) ) {
    			exit;
			}
		}
	}
?>

<div>
	
	callback
</div>